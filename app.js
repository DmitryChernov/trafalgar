let nav = document.getElementById('header__nav-ul');
let navBtn = document.getElementById('header__nav-button');
let navBtnClose = document.getElementById('header__nav-button-close');

navBtn.onclick = () => {
    nav.classList.add('header__nav-list--open')
}

navBtnClose.onclick = () => {
    nav.classList.remove('header__nav-list--open')
}

let nextBtn = document.getElementById("slider-next");
let prevBtn = document.getElementById("slider-previous");

let nextPrevFunction = function (eventNext) {
    let activeSlide = document.querySelector(".sec04__slide--order");
    let labelActive = document.querySelector(".navRadio__label--checked");
    labelActive.classList.remove("navRadio__label--checked");
    let idNext;
    let newActiveLabel;
    if (eventNext == true) {
        idNext = Number(activeSlide.id.slice(-1)) + 1;
    } else {
        idNext = Number(activeSlide.id.slice(-1)) - 1;
    }
    let nextSlide = document.getElementById("slide" + idNext);
    if (nextSlide !== null) {
        activeSlide.classList.remove("sec04__slide--order");
        nextSlide.classList.add("sec04__slide--order");
        newActiveLabel = document.getElementById(idNext)
    } else if (activeSlide.id == "slide1") {
        nextSlide = document.getElementById("slide4");
        activeSlide.classList.remove("sec04__slide--order");
        nextSlide.classList.add("sec04__slide--order");
        newActiveLabel = document.getElementById("4")
    } else if (activeSlide.id == "slide4") {
        nextSlide = document.getElementById("slide1");
        activeSlide.classList.remove("sec04__slide--order");
        nextSlide.classList.add("sec04__slide--order");
        newActiveLabel = document.getElementById("1");
    }
    let label = document.querySelector(`[for="${newActiveLabel.id}"]`);
    label.classList.add("navRadio__label--checked");
}

nextBtn.addEventListener("click", () => {
    nextPrevFunction(true)
})

prevBtn.addEventListener("click", () => {
    nextPrevFunction(false)
})

let slideInput = document.querySelectorAll(".navRadio__button--display");

for (let i = 0; i < slideInput.length; i++) {
    slideInput[i].addEventListener("click", () => {
        let activeSlide = document.querySelector(".sec04__slide--order");
        activeSlide.classList.remove("sec04__slide--order");
        let labelActive = document.querySelector(".navRadio__label--checked");
        labelActive.classList.remove("navRadio__label--checked");
        let label = document.querySelector(`[for="${slideInput[i].id}"]`);
        label.classList.add('navRadio__label--checked')
        let newActiveSlide = document.getElementById("slide" + slideInput[i].id);
        newActiveSlide.classList.add("sec04__slide--order");
    })
}